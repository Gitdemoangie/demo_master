package com.example.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	private DataController control = new DataController();

	
	@Test
	public void contextLoads() {
		DataBean data = control.calcular(new Double(10), new Double(10));
		assertEquals("Calculo", new Double(100), data.getTotal());
	}

}
